from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from django.contrib import messages
from django.views.generic.base import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin


class LoginPageView(TemplateView):

    template_name = "login.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('dashboard:dashboard')
        return self.render_to_response({})

    def post(self, request, *args, **kwargs):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_staff:
                user.save()
                login(request, user)
                return redirect('dashboard:dashboard')
            else:
                messages.error(request, "You're not and admin. Please login as one.")
                return self.render_to_response({})
        else:
            messages.error(request, "Incorrect Username or Password")
            return self.render_to_response({})

class LogoutView(LoginRequiredMixin, TemplateView):

    def get(self, request, *args, **kwargs):
        logout(request)
        return redirect('authentication:login')
