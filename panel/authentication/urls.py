from django.urls import path

from .views.authentication import LoginPageView, LogoutView

app_name = 'authentication'

urlpatterns = [
	path('login/', LoginPageView.as_view(), name='login'),
	path('logout/', LogoutView.as_view(), name='logout'),
]
