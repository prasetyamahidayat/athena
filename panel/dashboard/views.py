from django.views.generic.base import TemplateView
from django.http import JsonResponse
from django.contrib import messages
from django.shortcuts import render, redirect

class DashboardPageView(TemplateView):

    template_name = "dashboard.html"

    def get(self, request, *args, **kwargs):

        if request.user.is_authenticated:

            return self.render_to_response({
                'status' : "OKE" 
            })
        return redirect('authentication:login')
